import * as React from "react";

interface ITaskProps extends React.Props<ITaskProps> {
    Name?: string;
    Priority?: number;
}

class Task extends React.Component<ITaskProps, {}> {
    static defaultProps: ITaskProps;

    constructor(props: ITaskProps) {
        super(props);
    }
    
    render() {
        return(
            <div id="task">
                <h2>{this.props.Name}</h2>
                <p>{this.props.Priority}</p>  
            </div>);
    }
}

Task.defaultProps = { Name: "Daniil", Priority: 1 }
export default Task;